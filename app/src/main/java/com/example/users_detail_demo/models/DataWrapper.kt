package com.example.users_detail_demo.models

import com.google.gson.annotations.SerializedName

data class DataWrapper(
    @field:SerializedName("data")
    val person: Person
)