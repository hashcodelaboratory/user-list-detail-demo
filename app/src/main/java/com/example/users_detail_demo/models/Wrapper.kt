package com.example.users_detail_demo.models

import com.google.gson.annotations.SerializedName

data class Wrapper(
    @field:SerializedName("page")
    val page: Int,
    @field:SerializedName("per_page")
    val perPage: Int,
    @field:SerializedName("total")
    val total: Int,
    @field:SerializedName("total_pages")
    val totalPages: Int,
    @field:SerializedName("data")
    val users: List<Person>
)
