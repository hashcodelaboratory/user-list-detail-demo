package com.example.users_detail_demo.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "persons")
data class Person(
    @field:SerializedName("id")
    @PrimaryKey var id: Int,
    @field:SerializedName("first_name")
    var firstName: String,
    @field:SerializedName("last_name")
    var lastName: String,
    @field:SerializedName("email")
    var email: String,
    @field:SerializedName("avatar")
    var avatar: String
)