package com.example.users_detail_demo.viewModels

import android.app.AlertDialog
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.users_detail_demo.MainActivity
import com.example.users_detail_demo.dataSources.PeopleDataSourceManager
import com.example.users_detail_demo.fragments.PeopleList
import com.example.users_detail_demo.fragments.PersonDetail
import com.example.users_detail_demo.models.DataWrapper
import com.example.users_detail_demo.models.Person
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class PeopleViewModel @Inject constructor(val dataSourceManager: PeopleDataSourceManager) :
    ViewModel() {

    fun invalidateDataSource() = dataSourceManager.invalidateDataSource()

    fun getPeople(): LiveData<PagedList<Person>> {
        return dataSourceManager.peopleList
    }

    fun getPerson(personId: Int): LiveData<Person>? {
        return dataSourceManager.sourceLiveData.value?.repository?.getPerson(personId)
    }

    fun fetchPerson(personId: Int) {
        dataSourceManager.sourceLiveData.value?.apiService?.getUser(personId)?.enqueue(object : Callback<DataWrapper> {
            override fun onResponse(call: Call<DataWrapper>, response: Response<DataWrapper>) {
                if (response.isSuccessful) {
                    val apiResponse = response.body()!!
                    val person = apiResponse.person
                    dataSourceManager.sourceLiveData.value?.repository?.save(person)
                    PersonDetail.applicationActivity()?.runOnUiThread {
                        Toast.makeText(PersonDetail.applicationContext(), "User fetched.", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<DataWrapper>, t: Throwable) {
                val dialogBuilder = AlertDialog.Builder(PersonDetail.applicationActivity())
                dialogBuilder.setTitle("Fetching failure")
                dialogBuilder.setMessage(t.message + "\n\nDo you want to repeat your action ?")
                dialogBuilder.setPositiveButton("YES") {_,_ ->
                    fetchPerson(personId)
                }
                dialogBuilder.setNegativeButton("NO") {_,_ ->

                }

                dialogBuilder.setNeutralButton("Cancel") {_,_ ->

                }

                dialogBuilder.create().show()
            }
        })
    }
}