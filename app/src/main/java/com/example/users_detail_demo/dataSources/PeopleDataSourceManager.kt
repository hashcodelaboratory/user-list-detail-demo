package com.example.users_detail_demo.dataSources

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.Config
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.users_detail_demo.models.Person
import com.example.users_detail_demo.room.PeopleRepository
import com.example.users_detail_demo.services.ApiService
import javax.inject.Inject

class PeopleDataSourceManager @Inject constructor(
    private val repository: PeopleRepository,
    private val apiService: ApiService
) : DataSource.Factory<Int, Person>() {

    var reload = false

    private val pagingConfig = Config(
        pageSize = PeopleDataSource.PAGE_SIZE,
        prefetchDistance = 150,
        enablePlaceholders = true
    )

    var peopleList: LiveData<PagedList<Person>> =
        LivePagedListBuilder(this, pagingConfig).build()

    fun invalidateDataSource() = sourceLiveData.value?.invalidate()

    val sourceLiveData = MutableLiveData<PeopleDataSource>()

    override fun create(): DataSource<Int, Person> {
        val latestSource = PeopleDataSource(repository, apiService)
        latestSource.reload = reload
        sourceLiveData.postValue(latestSource)
        return latestSource
    }
}