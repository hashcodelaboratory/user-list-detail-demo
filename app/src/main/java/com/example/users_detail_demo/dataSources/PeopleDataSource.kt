package com.example.users_detail_demo.dataSources

import android.app.AlertDialog
import android.widget.Toast
import androidx.paging.PageKeyedDataSource
import com.example.users_detail_demo.fragments.PeopleList
import com.example.users_detail_demo.models.Person
import com.example.users_detail_demo.models.Wrapper
import com.example.users_detail_demo.room.PeopleRepository
import com.example.users_detail_demo.services.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class PeopleDataSource @Inject constructor(
    val repository: PeopleRepository,
    val apiService: ApiService
) : PageKeyedDataSource<Int, Person>() {

    var reload = false

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Person>) {
        val people = repository.getPeople(PAGE_SIZE, (if (params.key > 0) params.key - 1 else 0) * PAGE_SIZE)
        if (people.isNotEmpty() && !reload) {
            val key = if (params.key > 0) params.key - 1 else 0
            callback.onResult(people, key)
            PeopleList.applicationActivity()?.runOnUiThread {
                Toast.makeText(PeopleList.applicationContext(), "Users loaded.", Toast.LENGTH_SHORT).show()
            }

            return
        }
        apiService.getUsers(params.key, PAGE_SIZE).enqueue(object : Callback<Wrapper> {
            override fun onResponse(call: Call<Wrapper>, response: Response<Wrapper>) {
                if (response.isSuccessful) {
                    val apiResponse = response.body()!!
                    val responseItems = apiResponse.users
                    val key = if (params.key > 1) params.key - 1 else 0
                    repository.saveAll(responseItems)
                    callback.onResult(responseItems, key)
                    PeopleList.applicationActivity()?.runOnUiThread {
                        Toast.makeText(PeopleList.applicationContext(), "Users fetched.", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<Wrapper>, t: Throwable) {
                val dialogBuilder = AlertDialog.Builder(PeopleList.applicationContext())
                dialogBuilder.setTitle("Loading before failure")
                dialogBuilder.setMessage(t.message + "\n\nDo you want to repeat your action ?")
                dialogBuilder.setPositiveButton("YES") {_,_ ->
                    loadBefore(params, callback)
                }
                dialogBuilder.setNegativeButton("NO") {_,_ ->

                }

                dialogBuilder.setNeutralButton("Cancel") {_,_ ->

                }

                dialogBuilder.create().show()
            }
        })
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Person>
    ) {
        val people = repository.getPeople(PAGE_SIZE, 0)
        if (people.isNotEmpty() && !reload) {
            callback.onResult(people, null, FIRST_PAGE + 1)
            PeopleList.applicationActivity()?.runOnUiThread {
                Toast.makeText(PeopleList.applicationContext(), "Users loaded.", Toast.LENGTH_SHORT).show()
            }
            return
        }
        apiService.getUsers(FIRST_PAGE, PAGE_SIZE).enqueue(object : Callback<Wrapper> {
            override fun onResponse(call: Call<Wrapper>, response: Response<Wrapper>) {
                if (response.isSuccessful) {
                    val apiResponse = response.body()!!
                    val responseItems = apiResponse.users

                    repository.deletePeople()
                    repository.saveAll(responseItems)
                    callback.onResult(responseItems, null, FIRST_PAGE + 1)
                    PeopleList.applicationActivity()?.runOnUiThread {
                        Toast.makeText(PeopleList.applicationContext(), "Users fetched.", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<Wrapper>, t: Throwable) {
                val dialogBuilder = AlertDialog.Builder(PeopleList.applicationContext())
                dialogBuilder.setTitle("Loading initial failure")
                dialogBuilder.setMessage(t.message + "\n\nDo you want to repeat your action ?")
                dialogBuilder.setPositiveButton("YES") {_,_ ->
                    loadInitial(params, callback)
                }
                dialogBuilder.setNegativeButton("NO") {_,_ ->

                }

                dialogBuilder.setNeutralButton("Cancel") {_,_ ->

                }

                dialogBuilder.create().show()
            }
        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Person>) {
        val people = repository.getPeople(PAGE_SIZE, (params.key - 1) * PAGE_SIZE)
        if (people.isNotEmpty() && !reload) {
            val key = params.key + 1
            callback.onResult(people, key)
            PeopleList.applicationActivity()?.runOnUiThread {
                Toast.makeText(PeopleList.applicationContext(), "Users loaded.", Toast.LENGTH_SHORT).show()
            }
            return
        }
        apiService.getUsers(params.key, PAGE_SIZE).enqueue(object : Callback<Wrapper> {
            override fun onResponse(call: Call<Wrapper>, response: Response<Wrapper>) {
                if (response.isSuccessful) {
                    val apiResponse = response.body()!!
                    val responseItems = apiResponse.users
                    val key = params.key + 1
                    if (responseItems.isEmpty()) {
                        reload = false
                    }
                    repository.saveAll(responseItems)
                    callback.onResult(responseItems, key)
                    PeopleList.applicationActivity()?.runOnUiThread {
                        Toast.makeText(PeopleList.applicationContext(), "Users fetched.", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<Wrapper>, t: Throwable) {
                val dialogBuilder = AlertDialog.Builder(PeopleList.applicationContext())
                dialogBuilder.setTitle("Loading after failure")
                dialogBuilder.setMessage(t.message + "\n\nDo you want to repeat your action ?")
                dialogBuilder.setPositiveButton("YES") {_,_ ->
                    loadAfter(params, callback)
                }
                dialogBuilder.setNegativeButton("NO") {_,_ ->

                }

                dialogBuilder.setNeutralButton("Cancel") {_,_ ->

                }

                dialogBuilder.create().show()
            }
        })
    }

    companion object {
        const val PAGE_SIZE = 5
        const val FIRST_PAGE = 1
    }
}