package com.example.users_detail_demo.factories

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import javax.inject.Inject
import javax.inject.Provider

class PeopleFragmentFactory @Inject constructor(
    private val fragmentProviders: Map<Class<out Fragment>, @JvmSuppressWildcards Provider<Fragment>>
) : FragmentFactory() {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        val clazz =
            loadFragmentClass(classLoader, className)

        val provider = fragmentProviders[clazz] ?: fragmentProviders.entries.first { clazz.isAssignableFrom(it.key) }.value

        return provider.get()
    }
}