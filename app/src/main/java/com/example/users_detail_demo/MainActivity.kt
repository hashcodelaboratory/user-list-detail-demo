package com.example.users_detail_demo

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.users_detail_demo.fragments.PeopleList
import com.example.users_detail_demo.modules.PeoplePersistenceModule
import javax.inject.Inject

open class MainActivity : AppCompatActivity() {

    @Inject
    protected lateinit var factory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        val dagger: PeopleComponent = DaggerPeopleComponent.builder()
            .peoplePersistenceModule(PeoplePersistenceModule(application))
            .build()

        supportFragmentManager.fragmentFactory = dagger.fragmentFactory()

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            val fragment = supportFragmentManager.fragmentFactory.instantiate(
                classLoader,
                PeopleList::class.java.canonicalName!!
            )

            fragment.let {
                supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container, it
                ).commit()
            }
        }
    }
}
