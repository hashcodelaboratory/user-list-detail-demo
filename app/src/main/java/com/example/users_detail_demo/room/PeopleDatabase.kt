package com.example.users_detail_demo.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.users_detail_demo.models.Person

@Database(entities = [Person::class], version = 1, exportSchema = false)
abstract class PeopleDatabase : RoomDatabase() {

    abstract fun personDao(): PeopleDao

    companion object {
        const val DATABASE_NAME = "people"
    }
}