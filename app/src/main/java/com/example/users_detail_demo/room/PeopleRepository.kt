package com.example.users_detail_demo.room

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.users_detail_demo.models.Person
import javax.inject.Inject

class PeopleRepository @Inject constructor(
    private val peopleDao: PeopleDao
) {

    fun save(person: Person) {
        SavePersonTask(peopleDao).execute(person)
    }

    fun saveAll(persons: List<Person>) {
        SavePersonsTask(peopleDao).execute(persons)
    }

    fun deletePeople() {
        DeletePeopleTask(peopleDao).execute()
    }

    fun getPerson(personId: Int): LiveData<Person> {
        return peopleDao.find(personId)
    }

    fun getPeople(limit: Int, offset: Int): List<Person> {
        return peopleDao.getPeople(limit, offset)
    }

    private class SavePersonTask(val peopleDao: PeopleDao) : AsyncTask<Person, Unit, Unit>() {

        override fun doInBackground(vararg params: Person?) {
            peopleDao.save(params[0]!!)
        }
    }

    private class SavePersonsTask(val peopleDao: PeopleDao) :
        AsyncTask<List<Person>, Unit, Unit>() {

        override fun doInBackground(vararg params: List<Person>?) {
            peopleDao.saveAll(params[0]!!)
        }
    }

    private class DeletePeopleTask(val peopleDao: PeopleDao) : AsyncTask<Unit, Unit, Unit>() {

        override fun doInBackground(vararg params: Unit?) {
            peopleDao.deleteAllPersons()
        }
    }
}