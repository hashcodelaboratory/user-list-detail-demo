package com.example.users_detail_demo.room

import androidx.lifecycle.LiveData
import androidx.paging.DataSource.Factory
import androidx.room.*
import com.example.users_detail_demo.models.Person

@Dao
interface PeopleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(person: List<Person>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(person: Person)

    @Update
    fun update(person: Person)

    @Delete
    fun delete(person: Person)

    @Query("DELETE FROM persons")
    fun deleteAllPersons()

    @Query("SELECT * FROM persons")
    fun findAll(): LiveData<List<Person>>

    @Query("SELECT * FROM persons WHERE id = :personId LIMIT 1")
    fun find(personId: Int): LiveData<Person>

    @Query("SELECT * FROM persons")
    fun getPeople(): Factory<Int, Person>

    @Query("SELECT * FROM persons ORDER BY id LIMIT :limit OFFSET :offset")
    fun getPeople(limit: Int, offset: Int): List<Person>

}