package com.example.users_detail_demo.services

import com.example.users_detail_demo.models.DataWrapper
import com.example.users_detail_demo.models.Wrapper
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("users")
    fun getUsers(@Query("page") page: Int, @Query("per_page") perPage: Int): Call<Wrapper>

    @GET("users/{id}")
    fun getUser(@Path("id") id: Int): Call<DataWrapper>
}