package com.example.users_detail_demo.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.users_detail_demo.MainActivity
import com.example.users_detail_demo.R
import com.example.users_detail_demo.viewModels.PeopleViewModel
import com.example.users_detail_demo.models.Person
import kotlinx.android.synthetic.main.fragment_user_detail.*
import kotlinx.android.synthetic.main.fragment_users_list.view.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class PersonDetail @Inject constructor(
    private val factory: ViewModelProvider.Factory
) : Fragment() {

    private val peopleViewModel: PeopleViewModel by lazy { factory.create(
        PeopleViewModel::class.java) }

    init {
        instanceDetail = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user_detail, container, false)

        when (arguments != null) {
            true -> {
                val id = arguments!!["id"].toString().toInt()
                val person = peopleViewModel.getPerson(id)

                view.swipe_refresh_layout.setOnRefreshListener {
                    peopleViewModel.fetchPerson(id)
                    view.swipe_refresh_layout.isRefreshing = false
                }

                person?.observe(viewLifecycleOwner, Observer<Person> { observedPerson ->
                    if (observedPerson == null) {
                        peopleViewModel.fetchPerson(id)
                    } else {
                        email_detail.text = observedPerson.email
                        firstName_detail.text = observedPerson.firstName
                        surname_detail.text = observedPerson.lastName
                        Glide.with(context).load(observedPerson.avatar).into(avatar_userDetail)
                    }
                })
                Toast.makeText(context, "Load", Toast.LENGTH_SHORT).show()
            }
        }

        return view
    }

    companion object {
        private var instanceDetail: PersonDetail? = null

        fun applicationContext() : Context? {
            return instanceDetail?.context
        }

        fun applicationActivity(): FragmentActivity? {
            return instanceDetail?.activity
        }
    }
}
