package com.example.users_detail_demo.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.users_detail_demo.R
import com.example.users_detail_demo.adapters.PeopleAdapter
import com.example.users_detail_demo.viewModels.PeopleViewModel
import kotlinx.android.synthetic.main.fragment_users_list.view.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class PeopleList @Inject constructor(private val factory: ViewModelProvider.Factory) : Fragment() {

    private val peopleViewModel: PeopleViewModel by lazy {
        factory.create(
            PeopleViewModel::class.java
        )
    }

    init {
        instance = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_users_list, container, false)

        val adapter = PeopleAdapter()

        view.recycler_view.layoutManager = LinearLayoutManager(context)

        peopleViewModel.getPeople().observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        view.recycler_view.adapter = adapter

        view.swipe_refresh_layout.setOnRefreshListener {
            peopleViewModel.dataSourceManager.reload = true
            peopleViewModel.invalidateDataSource()
            view.swipe_refresh_layout.isRefreshing = false
        }

        return view
    }

    companion object {
        private var instance: PeopleList? = null

        fun applicationContext() : Context? {
            return instance?.context
        }

        fun applicationActivity(): FragmentActivity? {
            return instance?.activity
        }
    }
}
