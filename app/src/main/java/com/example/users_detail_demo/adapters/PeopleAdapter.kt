package com.example.users_detail_demo.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.users_detail_demo.R
import com.example.users_detail_demo.fragments.PersonDetail
import com.example.users_detail_demo.models.Person
import kotlinx.android.synthetic.main.list_item.view.*

class PeopleAdapter :
    PagedListAdapter<Person, PeopleAdapter.PersonViewHolder>(PersonDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {

        return PersonViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val person = getItem(position)

        person?.let {
            holder.bind(it)
        }
    }

    inner class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewListName: TextView = itemView.text_view_name
        var textViewListSurname: TextView = itemView.text_view_surname
        var textViewListEmail: TextView = itemView.text_view_email_list
        var imageViewLogo: ImageView = itemView.image_view_logo
        var cardView: CardView = itemView.card_view

        fun bind(user: Person) {
            Glide.with(imageViewLogo.context).load(user.avatar).into(imageViewLogo)
            textViewListName.text = user.firstName
            textViewListSurname.text = user.lastName
            textViewListEmail.text = user.email

            cardView.setOnClickListener {

                val fragment =
                    (itemView.context as AppCompatActivity).supportFragmentManager.fragmentFactory.instantiate(
                        itemView.context.classLoader,
                        PersonDetail::class.java.canonicalName!!
                    )

                fragment.let {
                    val args = Bundle()
                    args.putInt("id", user.id)
                    it.arguments = args
                    (itemView.context as AppCompatActivity).supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, it).addToBackStack(null).commit()
                }
            }
        }
    }
}