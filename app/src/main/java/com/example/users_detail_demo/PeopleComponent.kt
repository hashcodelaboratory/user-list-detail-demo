package com.example.users_detail_demo

import androidx.fragment.app.FragmentFactory
import com.example.users_detail_demo.modules.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [PeoplePersistenceModule::class, ApplicationModule::class, PeopleFragmentFactoryModule::class, PeopleViewModelModule::class, NetworkModule::class, DataSourceModule::class])
interface PeopleComponent {
    fun fragmentFactory(): FragmentFactory
}