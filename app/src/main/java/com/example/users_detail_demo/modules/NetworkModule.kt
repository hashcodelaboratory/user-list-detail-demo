package com.example.users_detail_demo.modules

import com.example.users_detail_demo.services.ApiService
import com.example.users_detail_demo.services.ApiServiceBuilder
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
@Suppress("unused")
object NetworkModule {

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideApiService(): ApiService {
        return ApiServiceBuilder.buildService(ApiService::class.java)
    }

}