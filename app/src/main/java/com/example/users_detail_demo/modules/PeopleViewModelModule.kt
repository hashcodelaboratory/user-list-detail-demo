package com.example.users_detail_demo.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.users_detail_demo.factories.PeopleViewModelFactory
import com.example.users_detail_demo.modules.annotations.ViewModelKey
import com.example.users_detail_demo.viewModels.PeopleViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PeopleViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: PeopleViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(PeopleViewModel::class)
    abstract fun bindPeopleViewModel(peopleViewModel: PeopleViewModel): ViewModel
}