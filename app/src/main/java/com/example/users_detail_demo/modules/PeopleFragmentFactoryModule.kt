package com.example.users_detail_demo.modules

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.example.users_detail_demo.factories.PeopleFragmentFactory
import com.example.users_detail_demo.fragments.PeopleList
import com.example.users_detail_demo.fragments.PersonDetail
import com.example.users_detail_demo.modules.annotations.FragmentKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PeopleFragmentFactoryModule {

    @Binds
    abstract fun bindFragmentFactory(fragmentFactory: PeopleFragmentFactory): FragmentFactory

    @Binds
    @IntoMap
    @FragmentKey(PeopleList::class)
    abstract fun bindPeopleFragment(peopleList: PeopleList): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PersonDetail::class)
    abstract fun bindPersonDetailFragment(peopleList: PersonDetail): Fragment
}