package com.example.users_detail_demo.modules

import com.example.users_detail_demo.dataSources.PeopleDataSourceManager
import com.example.users_detail_demo.room.PeopleRepository
import com.example.users_detail_demo.services.ApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataSourceModule {

    @Singleton
    @Provides
    fun provideDataSourceFactory(
        repository: PeopleRepository,
        apiService: ApiService
    ): PeopleDataSourceManager {
        return PeopleDataSourceManager(
            repository,
            apiService
        )
    }
}