package com.example.users_detail_demo.modules

import android.app.Application
import androidx.room.Room
import com.example.users_detail_demo.room.PeopleDao
import com.example.users_detail_demo.room.PeopleDatabase
import com.example.users_detail_demo.room.PeopleRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PeoplePersistenceModule(private val application: Application) {

    @Singleton
    @Provides
    fun providePeopleDatabase(): PeopleDatabase {
        return Room.databaseBuilder(
            application,
            PeopleDatabase::class.java,
            PeopleDatabase.DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

    @Singleton
    @Provides
    fun providePeopleDao(database: PeopleDatabase): PeopleDao {
        return database.personDao()
    }

    @Singleton
    @Provides
    fun provideRepository(peopleDao: PeopleDao): PeopleRepository {
        return PeopleRepository(peopleDao)
    }
}